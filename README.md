# CSV_to_XLS_Converter

It is a CSV to XLS converter. The software comes with a graphical interface!

**Dependencies**
*  Python 3
The following Python libraries
*  tkinter
*  csv
*  xlwt

**Instructions**

Just run with Python the file CSVtoXLS.py and a new window will appear. It is the graphical interface of the program.

In the new window press the "Choose a file" button to browse the CSV file you want to convert. Then type the name of the new XLS file. Finally press the "Convert" button.  The new file is in the same file as this programm')

![alt text](https://gitlab.com/charalamm/csv_to_xls_converter/raw/master/Image.png)