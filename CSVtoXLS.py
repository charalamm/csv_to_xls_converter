import csv
import xlwt
import tkinter as tk
from tkinter import filedialog
import os

window=tk.Tk()
window.geometry("500x275")
window.title('CSV to XLS Converter')

instructions=tk.Label(window,anchor='nw',justify='left',text='INSTRUCTIONS: \n Use the "Choose a file" button to browse the CSV file you want to convert. \n Then type the name of the new XLS file.\n Finally press the "Convert" button. \n The new file is in the same file as this programm')
instructions.grid(row=0, column=0)

dic={'CSV':'None','XLS':'None'}

CSVLabel=tk.Label(window,anchor='sw',text='Name of CSV File to be converted:')
CSVLabel.grid(pady=(10,0))
CSVBox=tk.Entry(window,exportselection=0)
CSVBox.grid(row=2, column=0)

def Browse():
    file = tk.filedialog.askopenfilename(parent=window,title='Choose a file',filetypes=(("csv files","*.csv"),("all files","*.*")))
    if file != None:
        CSVBox.delete(0,len(CSVBox.get()))
        CSVBox.insert(0,file)
        
Browse_CSV_button = tk.Button(window,text='Browse .csv',command=Browse)
Browse_CSV_button.grid(row=4, column=0)

XLSLabel=tk.Label(window,anchor='sw',text='Name of XLS file to be produced (ex. Example.xls):')
XLSLabel.grid(row=5, column=0,pady=(10,0))
XLSBox=tk.Entry(window,exportselection=0)
XLSBox.grid(row=6, column=0)

def Convert():
    dic['CSV']=CSVBox.get()
    dic['XLS']=XLSBox.get()
    
    wb = xlwt.Workbook()
    sh = wb.add_sheet('Sheet 0')

    # CSV = 'CSV Example.csv'
    with open(dic['CSV'], 'r') as f:
        reader = csv.reader(f)
        for r, row in enumerate(reader):
            for c, val in enumerate(row):
                sh.write(r, c, val)
            
    #XLS='XLS Example.xls'
    wb.save(dic['XLS']) 

Convert_button = tk.Button(window,text='Convert',command=Convert)
Convert_button.grid(row=7,column=0,pady=(10,0))


window.mainloop()
